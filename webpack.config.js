const path = require('path');
const webpack = require('webpack');

module.exports = {

    target: 'node',

    entry: {
        'simple.rest.api.umd': './simple.rest.api.ts'
    },

    output: {
        publicPath: '/',
        path: path.resolve(process.cwd(), 'dist'),
        filename: '[name].js',
        sourceMapFilename: '[file].map',
        library: '@siplay/simple-rest-api',
        libraryTarget: 'umd',
        umdNamedDefine: true
    },

    resolve: {
        extensions: ['.ts', '.js']
    },

    devtool: 'source-map',

    module: {
        loaders: [
            {
                test: /\.ts$/,
                loader: 'awesome-typescript-loader',
                exclude: /node_modules/
            }
        ]
    },

    plugins: [
        new webpack.ProgressPlugin()
    ]
};