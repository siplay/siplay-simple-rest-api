# SIPlay Simple REST API

This API is intended to be used for temporary uses like code exercises and rapid prototyping. It allows completely
arbitrary data to be defined at nearly any desired endpoint.

* The `Authorization` header is required for every request. The header value may be any string and is used as a user id.
* The server supports all operations on any path under `/api`, allowing you define your own arbitrary endpoints.
* The API uses the `id` key on resources as an identifier.
* All responses except `DELETE` will include a HAL HATEOAS-style `_links` property, including a `self` link to the resource.
* Data is stored in memory, and is not persisted once the server process stops.

### Creating Data
The HTTP methods `PUT` and `POST` may be used to create data in the API.

#### POST
Use `POST` when creating a collection of resources. For example, the request
`POST http://localhost:3000/api/my-resources` with a JSON body will result in a resource being created at
`http://localhost:3000/api/my-resources/randomly-assigned-id`. Successful requests will return the HTTP
Status `201 Created`, and the resource including its new `id`, and a `_links` property.

Multiple `HTTP POST` requests to the same endpoint will result in one resource being created for each
request.

#### PUT
Use `PUT` when creating a resource at a specific URI. For example, the request
`PUT http://localhost:3000/api/my-resources/12345` with a JSON boy will result in the resource at that
URI either being created if it does not already exist, or replaced if it does. If the resource is being
created, the server will respond with `201` instead of `200`.

The API will prevent any `PUT` requests to a uri previously used for a `POST`, since creating a resource
at that URI would break querying for the resource collection.

### Updating Data
The HTTP methods `PUT` and `PATCH` may be used to update data in the API.

#### PUT
Use `PUT` to create or update the entire resource. If a resource already exists at the request URI, it
will be replaced by the request body. Otherwise, it will be created.

#### PATCH
Use `PATCH` to update an existing resource with a partial body. The resource must already exist.

### Retrieving Data
Sending a `GET` request to a resource URI (e.g. one provided from a `self` link as mentioned above) will
retrieve that resource.

Sending a `GET` request to a resource collection URI (e.g. one previous used to send one or more `HTTP POST` requests)
will retrieve all resources created under that URI.

### Deleting Data
Sending a `DELETE` request to a resource URI (e.g. one provided from a `self` link as mentioned above) will
delete that resource, returning its content in the response.

Sending a `DELETE` request to a resource collection URI (e.g. one previous used to send one or more `HTTP POST` requests)
will delete all resources created under that URI, returning the collection of resources in the response.
